package com.seakale.study.socket.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Client {

	private AsynchronousSocketChannel channel;

	public Client(String host, int port) throws IOException,
			InterruptedException, ExecutionException {
		this.channel = AsynchronousSocketChannel.open();
		Future<?> future = channel.connect(new InetSocketAddress(host, port));
		future.get();
	}

	public void write(byte[] b) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(32);
		byteBuffer.put(b);
		byteBuffer.flip();
		channel.write(byteBuffer);
	}
	
	
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		Client client = new Client("localhost", 7788);
		client.write("Hello".getBytes());
	}
}
