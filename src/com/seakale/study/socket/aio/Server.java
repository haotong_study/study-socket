package com.seakale.study.socket.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ExecutionException;

public class Server {
	
	public void start(int port) throws IOException {
		
		// 打开通道
		final AsynchronousServerSocketChannel asc = AsynchronousServerSocketChannel
				.open();
		
		// 绑定端口
		asc.bind(new InetSocketAddress(port));
		
		// 注册接收Handler
		asc.accept(null,
				new CompletionHandler<AsynchronousSocketChannel, Void>() {
					public void completed(AsynchronousSocketChannel ch, Void att) {
						asc.accept(null, this);
						handle(ch);
					}

					public void failed(Throwable exc, Void att) {

					}
				});

		try {
			Thread.sleep(1000*8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void handle(AsynchronousSocketChannel ch) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(32);
		try {
			ch.read(byteBuffer).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		byteBuffer.flip();
		System.out.println(new String(byteBuffer.array()));
	}

	public static void main(String[] args) throws Exception {
		new Server().start(7788);
	}

}
