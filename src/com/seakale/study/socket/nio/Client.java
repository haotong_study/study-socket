package com.seakale.study.socket.nio;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String args[]) throws UnknownHostException,
			IOException {
		
		Socket sc = new Socket("127.0.0.1", 8888);
		OutputStream out = sc.getOutputStream();
		out.write("hello".getBytes());
		out.flush();
		sc.shutdownOutput();
		out.close();
		sc.close();
		
	}

}
