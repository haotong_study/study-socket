package com.seakale.study.socket.bio;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Client {
	
	public static void main(String[] args) throws Exception {
		
		Socket s = new Socket();
		s.connect(new InetSocketAddress("127.0.0.1", 8899));
		InputStream is = s.getInputStream();
		OutputStream os = s.getOutputStream();
		os.write("hello".getBytes());
		os.flush();
		s.close();
	}

}
