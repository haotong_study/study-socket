package com.seakale.study.socket.bio;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) throws Exception {
		
		ServerSocket ss = new ServerSocket();
		ss.bind(new InetSocketAddress(8899));
		
		while(true)
		{
			//接收socket需要排队
			Socket s = ss.accept();
			InputStream is = s.getInputStream();
			
			byte[] bts = new byte[is.available()];
			is.read(bts);
			
			System.out.println(new String(bts));
			
		}
		
		
		
	}

}
